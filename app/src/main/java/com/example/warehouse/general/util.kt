package com.example.warehouse.general

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.core.content.ContextCompat
import java.io.ByteArrayOutputStream
import java.io.IOException


fun bitmapToByteArray(bitmap: Bitmap): ByteArray {
    val stream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream)
    return stream.toByteArray()
}

fun drawableToByteArray(name:String,context: Context): ByteArray{
    val resources: Resources = context.getResources()
    val resourceId: Int = resources.getIdentifier(
        name, "drawable",
        context.getPackageName()
    )
    val d = ContextCompat.getDrawable(context, resourceId)
    val bitmap = (d as BitmapDrawable).bitmap
    val stream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
    return stream.toByteArray()
}

@Throws(IOException::class)
fun readBytes(context: Context, uri: Uri): ByteArray? =
    context.contentResolver.openInputStream(uri)?.buffered()?.use { it.readBytes() }