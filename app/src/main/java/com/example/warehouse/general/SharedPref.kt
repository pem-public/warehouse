package com.example.warehouse.general

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

class SharedPref(context: Context){

    companion object {
        private const val PREF_NAME = "Warehouse"
        private const val ROLE = "user_role"
        private const val USER_ID = "user_id"
        private const val USER_NAME = "user_name"
    }

    val sharedPref: SharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

    var userRole: Int
        get() = sharedPref.getInt(ROLE, USER_ROLE)
        set(value) = sharedPref.edit { putInt(ROLE,value)}

    var userId: Long
        get() = sharedPref.getLong(USER_ID, -1)
        set(value) = sharedPref.edit { putLong(USER_ID,value)}

    var userName: String
        get() = sharedPref.getString(USER_NAME, "") ?: ""
        set(value) = sharedPref.edit { putString(USER_NAME,value)}

    fun logout(){
        userRole = USER_ROLE
        userId = -1
        userName = ""
    }
}