package com.example.warehouse.general

import android.content.Context
import com.example.warehouse.data.model.CategoryData
import com.example.warehouse.data.model.ItemData
import com.example.warehouse.data.model.LoggedInUser

const val ADMIN_ROLE = 101
const val USER_ROLE = 102
const val REQUEST_IMAGE_CAPTURE = 1
const val REQUEST_GALLERY = 2

val PREPOPULATE_USER_DATA = listOf(
    LoggedInUser(displayName = "admin", password = "12345", role = ADMIN_ROLE),
    LoggedInUser(displayName = "user", password = "12345", role = USER_ROLE)
)

val PREPOPULATE_CATEGORY_DATA = listOf(
    CategoryData(categoryId = 1, categoryName = "Home Appliances"),
    CategoryData(categoryId = 2, categoryName = "Electronic Devices"),
    CategoryData(categoryId = 3, categoryName = "Computers")
)

fun getItemData(context: Context): List<ItemData> {
    return listOf(
        ItemData(
            itemName = "Twin cooling plus",
            categoryId = 1,
            image = drawableToByteArray("img_2700", context)
        ),
        ItemData(
            itemName = "Top loader WA8700K",
            categoryId = 1,
            image = drawableToByteArray("img_2701", context)
        ),
        ItemData(
            itemName = "Top load washing machine",
            categoryId = 1,
            image = drawableToByteArray("img_2702", context)
        ),
        ItemData(
            itemName = "Wave grill MW3500K",
            categoryId = 1,
            image = drawableToByteArray("img_2703", context)
        ),
        ItemData(
            itemName = "85 Q80T",
            categoryId = 2,
            image = drawableToByteArray("img_2704", context)
        ),
        ItemData(
            itemName = "75 TU8000",
            categoryId = 2,
            image = drawableToByteArray("img_2705", context)
        ),
        ItemData(
            itemName = "2.1Ch soundBar",
            categoryId = 2,
            image = drawableToByteArray("img_2706", context)
        ),
        ItemData(
            itemName = "HW-R450",
            categoryId = 2,
            image = drawableToByteArray("img_2707", context)
        ),
        ItemData(
            itemName = "Galaxy tab S6",
            categoryId = 3,
            image = drawableToByteArray("img_2708", context)
        ),
        ItemData(
            itemName = "Galaxy tab S7 Lite",
            categoryId = 3,
            image = drawableToByteArray("img_2709", context)
        ),
        ItemData(
            itemName = "Galaxy tab S7 plus",
            categoryId = 3,
            image = drawableToByteArray("img_2710", context)
        )
    )
}