package com.example.warehouse

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.example.warehouse.data.ItemDataSource
import com.example.warehouse.data.ItemRepository
import com.example.warehouse.data.LoginDataSource
import com.example.warehouse.data.LoginRepository
import com.example.warehouse.data.db.WarehouseDatabase
import com.example.warehouse.ui.item.viewmodel.ItemViewModelFactory
import com.example.warehouse.ui.login.viewmodel.LoginViewModelFactory
import java.util.concurrent.Executors

object Injection {

    private fun provideLoginCache(context: Context): LoginDataSource {
        val database = WarehouseDatabase.getInstance(context)
        return LoginDataSource(database.userDao(), Executors.newSingleThreadExecutor())
    }

    private fun provideLoginRepository(context: Context): LoginRepository {
        return LoginRepository(provideLoginCache(context))
    }

    fun provideLoginViewModelFactory(context: Context): ViewModelProvider.Factory {
        return LoginViewModelFactory(
            provideLoginRepository(context)
        )
    }

    private fun provideItemCache(context: Context): ItemDataSource {
        val database = WarehouseDatabase.getInstance(context)
        return ItemDataSource(database.itemDao(),database.categoryDao(), Executors.newSingleThreadExecutor())
    }

    private fun provideItemRepository(context: Context): ItemRepository {
        return ItemRepository(provideItemCache(context))
    }

    fun provideItemViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ItemViewModelFactory(
            provideItemRepository(context)
        )
    }
}