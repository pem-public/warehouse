package com.example.warehouse.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.warehouse.data.model.LoggedInUser

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: LoggedInUser): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(userList: List<LoggedInUser>): List<Long>

    @Query("SELECT * FROM UserTable WHERE displayName= :displayName AND password= :password")
    fun login(displayName: String, password: String): LiveData<LoggedInUser>

    @Query("SELECT * FROM UserTable ORDER BY userId DESC")
    fun retrieveAllUsers(): List<LoggedInUser>

}