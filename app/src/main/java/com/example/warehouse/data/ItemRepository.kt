package com.example.warehouse.data

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.warehouse.data.model.CategoryData
import com.example.warehouse.data.model.ItemData
import com.example.warehouse.data.model.ItemWithCategory

class ItemRepository(private val dataSource: ItemDataSource)  {

    companion object {
        private const val DATABASE_PAGE_SIZE = 5
    }

    fun getItemsFromDB(keyword:String): LiveData<PagedList<ItemWithCategory>> {
        val dataSourceFactory = dataSource.retrieveAllItems(keyword)
        return LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
            .build()
    }

    fun getCategories():LiveData<List<CategoryData>>{
        return dataSource.retrieveAllCategories()
    }

    fun saveItem(name:String,categoryId: Long,image: ByteArray) {
        dataSource.saveItem(ItemData(itemName = name,categoryId = categoryId,image = image))
    }

    fun editItem(itemId:Long,name:String,categoryId: Long,image: ByteArray) {
        dataSource.updateItem(ItemData(itemId = itemId,itemName = name,categoryId = categoryId,image = image))
    }
}