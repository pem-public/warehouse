package com.example.warehouse.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "UserTable")
data class LoggedInUser(
        @PrimaryKey(autoGenerate = true)
        val userId: Long = 0,
        val displayName: String,
        val password:String,
        val role: Int
)