package com.example.warehouse.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.warehouse.data.model.CategoryData

@Dao
interface CategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(category: CategoryData): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(categoryList: List<CategoryData>): List<Long>

    @Query("SELECT * FROM categorytable ORDER BY categoryId DESC")
    fun retrieveAll(): LiveData<List<CategoryData>>
}