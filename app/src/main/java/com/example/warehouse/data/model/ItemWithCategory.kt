package com.example.warehouse.data.model

import java.io.Serializable

class ItemWithCategory(
    val itemId: Long,
    val categoryId:Long,
    val itemName: String,
    val image: ByteArray,
    val categoryName: String
):Serializable