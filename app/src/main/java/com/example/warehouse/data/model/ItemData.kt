package com.example.warehouse.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ItemTable")
data class ItemData (
    @PrimaryKey(autoGenerate = true)
    val itemId: Long = 0,
    val itemName: String,
    val categoryId: Long,
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    val image: ByteArray
)