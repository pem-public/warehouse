package com.example.warehouse.data

import androidx.lifecycle.LiveData
import com.example.warehouse.data.model.LoggedInUser

class LoginRepository(private val dataSource: LoginDataSource) {

    fun login(username: String, password: String): LiveData<LoggedInUser> {
        return dataSource.login(username, password)
    }

}