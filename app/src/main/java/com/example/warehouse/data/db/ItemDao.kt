package com.example.warehouse.data.db

import androidx.paging.DataSource
import androidx.room.*
import com.example.warehouse.data.model.CategoryData
import com.example.warehouse.data.model.ItemData
import com.example.warehouse.data.model.ItemWithCategory

@Dao
interface ItemDao {

    @Update
    fun update(itemData: ItemData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(itemData: ItemData): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(itemList: List<ItemData>): List<Long>

    @Query("SELECT * FROM ItemTable INNER JOIN CategoryTable ON CategoryTable.categoryId = ItemTable.categoryId ORDER BY itemId DESC")
    fun retrieveAllItems(): DataSource.Factory<Int, ItemWithCategory>

    @Query("SELECT * FROM ItemTable INNER JOIN CategoryTable  ON ItemTable.categoryId = CategoryTable.categoryId WHERE ItemTable.itemName LIKE :keyword OR CategoryTable.categoryName LIKE :keyword ORDER BY itemId DESC")
    fun retrieveFilterItems(keyword:String?): DataSource.Factory<Int, ItemWithCategory>
}