package com.example.warehouse.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "CategoryTable")
data class CategoryData(
    @PrimaryKey(autoGenerate = true)
    val categoryId: Long = 0,
    val categoryName: String
){
    override fun toString(): String {
        return this.categoryName
    }
}