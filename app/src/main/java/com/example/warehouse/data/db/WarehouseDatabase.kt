package com.example.warehouse.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.warehouse.data.model.CategoryData
import com.example.warehouse.data.model.ItemData
import com.example.warehouse.data.model.LoggedInUser
import com.example.warehouse.general.*

@Database(
    entities = [LoggedInUser::class,ItemData::class,CategoryData::class],
    version = 1,
    exportSchema = false
)
abstract class WarehouseDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun itemDao(): ItemDao
    abstract fun categoryDao(): CategoryDao

    companion object {

        @Volatile
        private var INSTANCE: WarehouseDatabase? = null

        fun getInstance(context: Context): WarehouseDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext, WarehouseDatabase::class.java, "WarehouseDatabase.db"
            ).addCallback(object : Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    ioThread {
                        getInstance(context).userDao().insertAll(PREPOPULATE_USER_DATA)
                        getInstance(context).categoryDao().insertAll(PREPOPULATE_CATEGORY_DATA)
                        getInstance(context).itemDao().insertAll(getItemData(context))
                    }
                }
            }).build()
    }


}
