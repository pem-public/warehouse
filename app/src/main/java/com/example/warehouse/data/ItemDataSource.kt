package com.example.warehouse.data

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import com.example.warehouse.data.db.CategoryDao
import com.example.warehouse.data.db.ItemDao
import com.example.warehouse.data.model.CategoryData
import com.example.warehouse.data.model.ItemData
import com.example.warehouse.data.model.ItemWithCategory
import java.util.concurrent.Executor

class ItemDataSource(
    private val itemDao: ItemDao,
    private val categoryDao: CategoryDao,
    private val ioExecutor: Executor
) {

    fun retrieveAllItems(keyword:String): DataSource.Factory<Int, ItemWithCategory> {
        if(keyword == ""){
            return itemDao.retrieveAllItems()
        }else{
            return  itemDao.retrieveFilterItems("%"+keyword+"%")
        }
    }

    fun retrieveAllCategories(): LiveData<List<CategoryData>>{
        return categoryDao.retrieveAll()
    }

    fun saveItem(item:ItemData) {
        ioExecutor.execute {
            itemDao.insert(item)
        }
    }

    fun updateItem(item:ItemData) {
        ioExecutor.execute {
            itemDao.update(item)
        }
    }
}