package com.example.warehouse.data

import androidx.lifecycle.LiveData
import com.example.warehouse.data.db.UserDao
import com.example.warehouse.data.model.LoggedInUser
import java.util.concurrent.Executor

class LoginDataSource(
    private val userDao: UserDao,
    private val ioExecutor: Executor
) {

    fun login(username: String, password: String): LiveData<LoggedInUser> {
        return userDao.login(username, password)
    }
}