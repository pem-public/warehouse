package com.example.warehouse.ui.item

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.paging.PagedList
import androidx.recyclerview.widget.GridLayoutManager
import com.associate.associateandroidselftest.ui.ItemAdapter
import com.associate.associateandroidselftest.ui.ItemListener
import com.example.warehouse.Injection
import com.example.warehouse.R
import com.example.warehouse.data.model.ItemWithCategory
import com.example.warehouse.general.SharedPref
import com.example.warehouse.general.USER_ROLE
import com.example.warehouse.ui.item.viewmodel.ItemViewModel
import com.example.warehouse.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_items.*
import kotlinx.android.synthetic.main.content_items.*


class ItemsActivity : AppCompatActivity(), ItemListener {

    private lateinit var viewModel: ItemViewModel
    private val adapter = ItemAdapter(this)

    companion object {
        fun open(context: Context) {
            context.startActivity(Intent(context, ItemsActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_items)
        setSupportActionBar(findViewById(R.id.toolbar))

        title = SharedPref(this).userName

        if(SharedPref(this).userRole == USER_ROLE){
            fab.visibility = View.GONE
        }

        rv_item.layoutManager = GridLayoutManager(this,2)

        viewModel = ViewModelProvider(this, Injection.provideItemViewModelFactory(this))
            .get(ItemViewModel::class.java)

        fab.setOnClickListener {
            AddItemActivity.open(this,null)
        }
        initAdapter()
    }

    private fun initAdapter() {
        rv_item.adapter = adapter
        viewModel.itemObservable.observe(this, Observer<PagedList<ItemWithCategory>> {
            adapter.submitList(it)
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        val searchItem: MenuItem = menu.findItem(R.id.action_search)
        if (searchItem != null) {
            val searchView = searchItem.getActionView() as SearchView
            searchView.setOnCloseListener(object : SearchView.OnCloseListener {
                override fun onClose(): Boolean {
                    searchView.onActionViewCollapsed()
                    viewModel.filterItemByKeyword("")
                    return true
                }
            })

            val searchPlate = searchView.findViewById(androidx.appcompat.R.id.search_src_text) as EditText
            searchPlate.hint = "Search"
            val searchPlateView: View = searchView.findViewById(androidx.appcompat.R.id.search_plate)
            searchPlateView.setBackgroundColor(
                ContextCompat.getColor(
                    this,
                    android.R.color.transparent
                )
            )

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    viewModel.filterItemByKeyword(newText ?: "")
                    return false
                }
            })

            val searchManager =
                getSystemService(Context.SEARCH_SERVICE) as SearchManager
            searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> {
                SharedPref(this).logout()
                LoginActivity.open(this)
                finish()
            }
            else -> super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onItemEditPressed(item: ItemWithCategory) {
        AddItemActivity.open(this,item)
    }

}