package com.example.warehouse.ui.item.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.example.warehouse.R
import com.example.warehouse.data.model.CategoryData
import kotlinx.android.synthetic.main.spinner_layout.view.*

class CustomDropDown(context: Context, val catList: List<CategoryData>):
    ArrayAdapter<CategoryData>(context,0,catList) {

    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }
    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {
        val category = getItem(position)
        val view = recycledView ?: LayoutInflater.from(context).inflate(
            R.layout.spinner_layout,
            parent,
            false
        )
        view.catText.text = category?.categoryName
        return view
    }

    override fun getItemId(position: Int): Long {
        return catList[position].categoryId
    }
}