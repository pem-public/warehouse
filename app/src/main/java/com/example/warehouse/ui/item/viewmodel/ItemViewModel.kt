package com.example.warehouse.ui.item.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import com.example.warehouse.data.ItemRepository
import com.example.warehouse.data.model.ItemWithCategory

class ItemViewModel(private val repository: ItemRepository) : ViewModel() {

    val keyword: MutableLiveData<String> = MutableLiveData()

    init {
        keyword.value = ""
    }

    val itemObservable: LiveData<PagedList<ItemWithCategory>> = Transformations.switchMap(keyword) { param->
        repository.getItemsFromDB(keyword = keyword.value!!)
    }

    fun filterItemByKeyword(word: String) {
        keyword.value = word
    }

}