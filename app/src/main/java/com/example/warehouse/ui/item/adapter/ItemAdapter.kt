package com.associate.associateandroidselftest.ui

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.example.warehouse.data.model.ItemWithCategory

class ItemAdapter(private val listener: ItemListener) :
    PagedListAdapter<ItemWithCategory, ItemViewHolder>(LETTER_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder.create(parent, listener)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position) as ItemWithCategory)
    }

    companion object {
        private val LETTER_COMPARATOR = object : DiffUtil.ItemCallback<ItemWithCategory>() {
            override fun areItemsTheSame(oldItem: ItemWithCategory, newItem: ItemWithCategory): Boolean =
                oldItem.itemId == newItem.itemId

            override fun areContentsTheSame(oldItem: ItemWithCategory, newItem: ItemWithCategory): Boolean =
                oldItem.equals(newItem)
        }
    }
}