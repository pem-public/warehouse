package com.example.warehouse.ui.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.example.warehouse.R
import com.example.warehouse.general.SharedPref
import com.example.warehouse.ui.item.ItemsActivity
import com.example.warehouse.ui.login.LoginActivity


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler(Looper.getMainLooper()).postDelayed({
            val pref = SharedPref(this)
            if (pref.userId > -1) {
                ItemsActivity.open(this)
                finish()
            } else {
                LoginActivity.open(this)
                finish()
            }
        }, 2000)

    }
}