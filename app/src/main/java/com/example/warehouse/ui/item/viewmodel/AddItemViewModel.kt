package com.example.warehouse.ui.item.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.warehouse.data.ItemRepository
import com.example.warehouse.data.model.CategoryData

class AddItemViewModel(private val repository: ItemRepository) : ViewModel() {

    val categories: LiveData<List<CategoryData>>

    init {
        categories = repository.getCategories()
    }

    fun getAllCategories(): LiveData<List<CategoryData>> {
        return categories
    }


    fun saveItem(itemId:Long,name: String, categoryId: Long, image: ByteArray){
        if(itemId > -1){
            repository.editItem(itemId,name, categoryId, image)
        }else{
            repository.saveItem(name, categoryId, image)
        }
    }

}