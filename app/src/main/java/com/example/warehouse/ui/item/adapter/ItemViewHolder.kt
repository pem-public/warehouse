package com.associate.associateandroidselftest.ui

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.example.warehouse.R
import com.example.warehouse.data.model.ItemWithCategory
import com.example.warehouse.general.ADMIN_ROLE
import com.example.warehouse.general.SharedPref

class ItemViewHolder(val view: View, private val listener: ItemListener) :
    RecyclerView.ViewHolder(view) {

    private val txtTitle = view.findViewById<TextView>(R.id.txt_title)
    private val txtCategory = view.findViewById<TextView>(R.id.txt_category)
    private val imageView = view.findViewById<ImageView>(R.id.imageView2)
    private val itemMenu = view.findViewById<ImageView>(R.id.item_menu)

    private lateinit var item: ItemWithCategory

    companion object {
        fun create(parent: ViewGroup, listener: ItemListener): ItemViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_layout, parent, false)
            return ItemViewHolder(view, listener)
        }
    }

    fun bind(i: ItemWithCategory) {
        item = i
        txtTitle.text = i.itemName
        txtCategory.text = i.categoryName
        imageView.setImageBitmap(BitmapFactory.decodeByteArray(i.image, 0, i.image.size))

        if (SharedPref(view.context).userRole == ADMIN_ROLE)
            itemMenu.visibility = View.VISIBLE
        else
            itemMenu.visibility = View.GONE

        itemMenu.setOnClickListener {
            listener.onItemEditPressed(this.item)
        }
    }
}

interface ItemListener {
    fun onItemEditPressed(item:ItemWithCategory)
}