package com.example.warehouse.ui.item

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.warehouse.Injection
import com.example.warehouse.R
import com.example.warehouse.data.model.ItemWithCategory
import com.example.warehouse.general.REQUEST_GALLERY
import com.example.warehouse.general.REQUEST_IMAGE_CAPTURE
import com.example.warehouse.general.bitmapToByteArray
import com.example.warehouse.general.readBytes
import com.example.warehouse.ui.item.adapter.CustomDropDown
import com.example.warehouse.ui.item.viewmodel.AddItemViewModel
import kotlinx.android.synthetic.main.activity_add_item.*
import kotlinx.android.synthetic.main.add_item_view.*

class AddItemActivity : AppCompatActivity() {

    private val CAMERA_REQUEST_CODE = 101
    private val READ_STORAGE_REQUEST_CODE = 201

    private lateinit var viewModel: AddItemViewModel
    private var categoryId: Long = -1
    private lateinit var image: ByteArray
    private var itemId: Long = -1

    companion object {
        val ITEM = "ITEM"

        fun open(context: Context,item:ItemWithCategory?) {
            val intent = Intent(context, AddItemActivity::class.java)
            item?.let {
                intent.putExtra(ITEM,item)
            }
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_item)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val item = intent.getSerializableExtra(ITEM) as? ItemWithCategory

        item?.let {
            itemId = item.itemId
            etItemName.setText(item.itemName)
            categoryId = item.categoryId
            image = item.image
            imgView.setImageBitmap(BitmapFactory.decodeByteArray(item.image, 0, item.image.size))
        }

        viewModel = ViewModelProvider(this, Injection.provideItemViewModelFactory(this))
            .get(AddItemViewModel::class.java)

        viewModel.getAllCategories().observe(this, Observer {
            val adapter = CustomDropDown(this, it)
            categorySpinner.adapter = adapter
            for ((index, item) in it.withIndex()) {
                if(item.categoryId == categoryId){
                    categorySpinner.setSelection(index)
                    break
                }
            }
        })

        categorySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                categoryId = categorySpinner.adapter.getItemId(position)
            }
            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }

        btnCamera.setOnClickListener{
            checkPermissions(Manifest.permission.CAMERA,CAMERA_REQUEST_CODE)
        }

        btnGallery.setOnClickListener {
            checkPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,READ_STORAGE_REQUEST_CODE)
        }

        saveBtn.setOnClickListener{
            if(this::image.isInitialized && etItemName.text.toString() != "" && categoryId > -1){
                viewModel.saveItem(itemId,etItemName.text.toString(),categoryId,image = image)
                finish()
            }else{
                Toast.makeText(applicationContext, "Please check the information again.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onBackPressed()
        return true
    }

    private fun TakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    private fun openGalleryIntent() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, REQUEST_GALLERY)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            imgView.visibility = View.VISIBLE
            image = bitmapToByteArray(imageBitmap)
            imgView.setImageBitmap(imageBitmap)
        } else if (requestCode == REQUEST_GALLERY && resultCode == RESULT_OK) {
            image = readBytes(this,data?.data!!)!!
            imgView.setImageURI(data.data)
        }
    }

    private fun checkPermissions(permission:String,code:Int) {
        val myPermission = ContextCompat.checkSelfPermission(this,permission)
        if (myPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(permission), code)
        }else{
            if(code == CAMERA_REQUEST_CODE){
                TakePictureIntent()
            }else if(code == READ_STORAGE_REQUEST_CODE){
                openGalleryIntent()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (!grantResults.isEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                CAMERA_REQUEST_CODE -> {
                    TakePictureIntent()
                }
                READ_STORAGE_REQUEST_CODE -> {
                    openGalleryIntent()
                }
            }
        }
    }

}